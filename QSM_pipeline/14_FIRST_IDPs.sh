#!/bin/sh
#
#
# Description: Script to calculate 14 subcortical QSM IDPs and extract CSF voxels using ventricle mask
#
# Authors: Chaoyue Wang, Benjamin C. Tendler & Karla L. Miller
#
# Copyright 2021 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


#apply GDC to SWI and then tranform to T1 space

./fsl/6.0.3/bin/applywarp --rel -i ./SWI/QSM/QSM.nii.gz -r ./T1/T1.nii.gz -o ./QSM_to_T1.nii.gz -w ./SWI/SWI_TOTAL_MAG_orig_ud_warp.nii.gz --postmat=./SWI/SWI_to_T1.mat --interp=spline

#masking

./fsl/6.0.3/bin/fslmaths ./QSM_to_T1.nii.gz -mul ./T1/T1_brain_mask.nii.gz ./QSM_to_T1.nii.gz

#SWI to MNI

./fsl/6.0.3/bin/convertwarp --ref=./fsl/6.0.3/data/standard/MNI152_T1_1mm --warp1=./SWI/SWI_TOTAL_MAG_orig_ud_warp --midmat=./SWI/SWI_to_T1.mat --warp2=./T1/transforms/T1_to_MNI_warp --out=./SWI_to_MNI_warp

./fsl/6.0.3/bin/applywarp --rel -i ./SWI/QSM/QSM.nii.gz -r ./fsl/6.0.3/data/standard/MNI152_T1_1mm -w ./SWI_to_MNI_warp -o ./QSM_to_MNI --interp=spline

./fsl/6.0.3/bin/fslmaths ./QSM_to_MNI -mul /well/miller/users/gdy716/UKBB/MNI152_T1_1mm_brain_mask ./QSM_to_MNI

rm ./SWI_to_MNI_warp.nii.gz

#FSL_IDPs

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 9.9 -uthr 10.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Thalamus_L

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 48.9 -uthr 49.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Thalamus_R

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 10.9 -uthr 11.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Caudate_L

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 49.9 -uthr 50.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Caudate_R

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 11.9 -uthr 12.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Putamen_L

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 50.9 -uthr 51.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Putamen_R

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 12.9 -uthr 13.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Pallidum_L

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 51.9 -uthr 52.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Pallidum_R

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 16.9 -uthr 17.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Hippocampus_L

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 52.9 -uthr 53.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Hippocampus_R

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 17.9 -uthr 18.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Amygdala_L

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 53.9 -uthr 54.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Amygdala_R

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 25.9 -uthr 26.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Accumbens_L

./fsl/6.0.3/bin/fslmaths ./T1/T1_first/T1_first_all_fast_firstseg.nii.gz -thr 57.9 -uthr 58.1 -bin -kernel 2D -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_Accumbens_R

./fsl/6.0.3/bin/fslmaths ./T2_FLAIR/lesions/T1_unbiased_ventmask.nii.gz -kernel sphere 1 -ero -bin -mul ./QSM_to_T1.nii.gz ./ROI_QSM_CSF


echo `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Thalamus_L.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Thalamus_L.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Thalamus_R.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Thalamus_R.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Caudate_L.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Caudate_L.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Caudate_R.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Caudate_R.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Putamen_L.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Putamen_L.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Putamen_R.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Putamen_R.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Pallidum_L.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Pallidum_L.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Pallidum_R.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Pallidum_R.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Hippocampus_L.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Hippocampus_L.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Hippocampus_R.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Hippocampus_R.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Amygdala_L.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Amygdala_L.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Amygdala_R.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Amygdala_R.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Accumbens_L.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Accumbens_L.nii.gz -P 50` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Accumbens_R.nii.gz -M` `./fsl/6.0.3/bin/fslstats ./ROI_QSM_Accumbens_R.nii.gz -P 50` > ./QSM_14_subcortical_IDPs.txt

rm ./ROI_*R.nii.gz
rm ./ROI_*L.nii.gz
