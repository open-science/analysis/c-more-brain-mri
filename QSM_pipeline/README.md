# C-MORE QSM pipeline

* `QSM.m`:  This script is the main QSM processing pipeline. `weightedCombination.m` and `phasevariance_nonlin.m` are supporting functions.
*  `14_FIRST_IDPs.sh` : This script is to calculate 14 subcortical (FSL-FIRST segmented) IDPs following UKB pipeline and to extract CSF voxels.
* `Mix_Mod_CSF.py` : This script is to calculate CSF susceptibility values for QSM referencing.

## Requirements
- MATLAB R2017b or higher
- STI Suite - https://people.eecs.berkeley.edu/~chunlei.liu/software.html
- FSL - https://fsl.fmrib.ox.ac.uk/fsldownloads_registration
- Mixture Modelling tool - https://github.com/allera/One_Dim_Mixture_Models
