# C-MORE brain MRI analysis pipeline

Code for the analysis pipeline presented in the paper
> ["Adapting the UK Biobank brain imaging protocol and analysis pipeline for the C-MORE multi-organ study of COVID-19 survivors"](https://www.frontiersin.org/articles/10.3389/fneur.2021.753284/full)

Used in the project:
> "Capturing MultiORgan Effects of COVID-19’ (C-MORE) study". Details of the main multiorgan study are described [here](https://www.thelancet.com/journals/eclinm/article/PIIS2589-5370(20)30427-2/fulltext)

## How to cite
Griffanti L, Raman B, Alfaro-Almagro F, Filippini N, Cassar MP, Sheerin F, Okell TW, Kennedy McConnell FA, Chappell MA, Wang C, Arthofer C, Lange FJ, Andersson J, Mackay CE, Tunnicliffe EM, Rowland M, Neubauer S, Miller KL, Jezzard P, Smith SM. Adapting the UK Biobank Brain Imaging Protocol and Analysis Pipeline for the C-MORE Multi-Organ Study of COVID-19 Survivors. Front Neurol. 2021 Oct 29;12:753284. doi: 10.3389/fneur.2021.753284. PMID: 34777224; PMCID: PMC8586081.

Full paper openly available [here](https://www.frontiersin.org/articles/10.3389/fneur.2021.753284/full)


## Contents
* **T1-weighted.**
This sequence was exactly matched to the one used in UKB and images were processed using the [UKB pipeline](https://git.fmrib.ox.ac.uk/open-science/analysis/UK_biobank_pipeline_v_1)
*  **T2-FLAIR.** 
This sequence was closely matched to the one used in UKB.
Images were processed using the [UKB pipeline](https://git.fmrib.ox.ac.uk/open-science/analysis/UK_biobank_pipeline_v_1) to extract white matter hyperintensities (WMH).
Additional IDPs extracted in this study:
    * Size and FLAIR intensity of the olfactory bulb (OB) - see `OB_pipeline` for details
    * Periventricular WMH (PWMH) and deep WMH (DWMH) volumes using the `bianca_perivent_deep` script
* **Diffusion weighted MRI (dMRI).**
In this multiorgan protocol only 3 orthogonal diffusion directions were acquired. The [UKB pipeline](https://git.fmrib.ox.ac.uk/open-science/analysis/UK_biobank_pipeline_v_1) was therefore adapted for preprocessing the images and derive mean diffusivity (MD) IDPs. Scripts are in the `dMRI_pipeline` folder.  Preprocessing was performed using the `cmore_diff.sh` script. Average MD was then calculated within an average skeleton in standard space for each tract of the JHU atlas using the `cmore_tbss_md` script (modified from bb_tbss of the UKB pipeline) and also within the normal appearing white matter only (i.e. outside WMH) using the `cmore_tbss_md_NAWM` script.
* **Susceptibility-weighted MR Imaging (SWI).**
This sequence was closely matched to the one used in UKB.
Images were processed using the [UKB pipeline](https://git.fmrib.ox.ac.uk/open-science/analysis/UK_biobank_pipeline_v_1) to extract T2* in 14 structures.
Additional IDPs extracted in this study are quantitative susceptibility mapping (QSM) values, referenced to the CSF QSM value, from 14 structures. See `QSM_pipeline` for details
* **Arterial Spin Labelling (ASL).**
This sequence was not included in UKB at the time of this study. See `ASL_pipeline` for details on preprocessing pipeline and IDP extraction.

## Useful resources
[UKB pipeline code](https://git.fmrib.ox.ac.uk/open-science/analysis/UK_biobank_pipeline_v_1)

[UK Biobank Brain Imaging Documentation](https://biobank.ndph.ox.ac.uk/showcase/showcase/docs/brain_mri.pdf)

[UK Biobank Brain Imaging - Online Resources](https://www.fmrib.ox.ac.uk/ukbiobank/)
