#!/usr/bin/sh

# Description: Script to generate T2-FLAIR intensity values (raw and normalised for WM) from Olfactory Bulbs segmentation
# Author: Ludovica Griffanti
#
# Copyright 2021 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#main analysis directory containing subjects dirs
direc=$1
subjectslist="/path/to/subj/list/subj.txt"
outpath="/path/to/volume/measurement-output/"

for sub in `cat ${subjectslist}`; do
echo $sub
cd ${direc}/${sub}/T2_FLAIR
# assuming OB labels in subjects space are in ${sub}/T2_FLAIR dir and called ${sub}_ob_labels_trilinear.nii.gz

fslmaths ${sub}_ob_labels_trilinear.nii.gz -mul T2_FLAIR_unbiased.nii.gz ${sub}_FLAIR_mul_ob_trilinear.nii.gz
OBval=`fslstats ${sub}_FLAIR_mul_ob_trilinear.nii.gz -M`

# WM mask created duriing main UKB pipeline:
# https://git.fmrib.ox.ac.uk/falmagro/UK_biobank_pipeline_v_1/-/blob/master/bb_structural_pipeline/bb_struct_init
# $FSLDIR/bin/fslmaths T1_fast/T1_brain_pve_2.nii.gz -thr 0.5 -bin T1_fast/T1_brain_WM_mask.nii.gz

WMval=`fslstats T2_FLAIR_unbiased.nii.gz -k ${direc}/${sub}/T1/T1_fast/T1_brain_WM_mask.nii.gz -P 50`

#saving intensity, reference value and normalised reference
norm_OBval=`echo "scale=5; $OBval / $WMval" | bc -l`
echo $sub $OBval $WMval $norm_OBval >> ${outpath}/OB_FLAIR_values.txt
done