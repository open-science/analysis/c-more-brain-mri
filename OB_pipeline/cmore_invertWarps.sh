#!/bin/bash

# Author: Christoph Arthofer
# Copyright: FMRIB 2021

#$ -cwd -j y
#$ -r y

echo `date`: Executing task ${SGE_TASK_ID} of job ${JOB_ID} on `hostname` as user ${USER}
echo SGE_TASK_FIRST=${SGE_TASK_FIRST}, SGE_TASK_LAST=${SGE_TASK_LAST}, SGE_TASK_STEPSIZE=${SGE_TASK_STEPSIZE}

. ~/.bash_profile
. $MODULESHOME/init/bash
module load fsl/6.0.3

ID_PATH=$1
DATA_DIR=$2
NLN_DIR=$3
TEMP_DIR=$4

IMG_IN_ID=$(awk 'NR=='${SGE_TASK_ID} ${ID_PATH})

IMG_T2_HEAD_IN_PATH="${DATA_DIR}${IMG_IN_ID}/T2_FLAIR/T2_FLAIR_unbiased.nii.gz"
WARP_COMP_PATH="${NLN_DIR}${IMG_IN_ID}_to_template_warp_T2_composed.nii.gz"
WARP_COMP_INVERSE_PATH="${NLN_DIR}${IMG_IN_ID}_to_template_warp_T2_composed_inverse.nii.gz"
echo "invwarp -r ${IMG_T2_HEAD_IN_PATH} -w ${WARP_COMP_PATH} -o ${WARP_COMP_INVERSE_PATH}"
invwarp -r ${IMG_T2_HEAD_IN_PATH} -w ${WARP_COMP_PATH} -o ${WARP_COMP_INVERSE_PATH}


LABEL_IN_PATH="${TEMP_DIR}OB_labels.nii.gz"
LABEL_OUT_PATH="${NLN_DIR}OB_labels_to_${IMG_IN_ID}.nii.gz"
echo "applywarp --ref=${IMG_T2_HEAD_IN_PATH} --in=${LABEL_IN_PATH} --warp=${WARP_COMP_INVERSE_PATH} --out=${LABEL_OUT_PATH} --interp=trilinear"
applywarp --ref=${IMG_T2_HEAD_IN_PATH} --in=${LABEL_IN_PATH} --warp=${WARP_COMP_INVERSE_PATH} --out=${LABEL_OUT_PATH} --interp=trilinear

LABEL_IN_PATH="${TEMP_DIR}T2_template_head.nii.gz"
LABEL_OUT_PATH="${NLN_DIR}T2_head_template_to_${IMG_IN_ID}.nii.gz"
echo "applywarp --ref=${IMG_T2_HEAD_IN_PATH} --in=${LABEL_IN_PATH} --warp=${WARP_COMP_INVERSE_PATH} --out=${LABEL_OUT_PATH} --interp=spline"
applywarp --ref=${IMG_T2_HEAD_IN_PATH} --in=${LABEL_IN_PATH} --warp=${WARP_COMP_INVERSE_PATH} --out=${LABEL_OUT_PATH} --interp=spline

rv=$?

echo `date`: task complete
exit $rv